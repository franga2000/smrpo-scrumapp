# Sprint retrospective meeting 1 (24.3.2024)

V sprintu 1 smo za hitrost sprinta določili 10 točk, kar nanese 60 ur. 
Odločili smo se implementirati uporabniške zgodbe, katerih ocene časovne 
zahtevnosti so skupaj nanesle 9 točk. Implementirali smo vse razen ene uporabniške zgodbe,
ki imajo skupno oceno 8 točk.
Po seštevku smo za te zgodbe porabili 68 ur, kar je rahlo več od napovedane hitrosti sprinta
in je hkrati tudi znatno več, kot so napovedale ocene časovne zahtevnosti. Znaten del
teh ur je bil porabljen za postavitev razvojnih orodij in seznanjanje z uporabljeno tehnologijo.

Produktni vodja je sprejel le del uporabniških zgodb: od implementiranih 8 točk
je sprejel le 3.5 točk, vendar je pomembno omeniti, da ocenjujemo, da bomo za popravke
porabili znatno manj časa kot za prvotno implementacijo. Hkrati omenjamo, da smo med
razvijanjem izbranih zgodb tudi delno implementirali zgodbe, katerih v tem sprintu
načeloma nismo razvijali.

Sprejetje tako majhnega dela zgodb nas je presenetilo, saj to pomeni, da nismo
dobro razumeli zahtev produktnega vodje. Med sestankom smo sicer dobili boljšo sliko
željene rešitve.

Naše ocene zahtevnosti zgodb so bile relativno dobre - nekaj jih je bilo rahlo podcenjenih,
nekaj pa rahlo precenjenih. Izjema je zgodba Prijava v sistem, ki je v fazi testiranja vzela
veliko časa, vendar to ni povezano s samo zahtevnostjo ali količino dela, ampak z dejstvom,
da se je ta zgodba testirala prva in je v ta čas všteto tudi postavljanje testnega orodja.

V naslednjem sprintu bomo za izboljšavo procesa bolj sodelovali s produktnim vodjo,
hitrost pa se bo zaradi pridobljenih izkušenj v uporabljenih tehnologijah povečala.
Prav tako se bo hitrost povečala zaradi zgoraj omenjenih delnih implementacij zgodb,
katerih v preteklem sprintu načeloma nismo implementirali.
