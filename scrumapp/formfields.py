from django import forms


class HTML5DateInput(forms.DateInput):
    def __init__(self):
        super().__init__(attrs={'type': 'date'}, format="%Y-%m-%d")

class HTML5DateTimeInput(forms.DateTimeInput):
    def __init__(self):
        super().__init__(attrs={'type': 'datetime'}, format="%Y-%m-%dT%H:%M")
