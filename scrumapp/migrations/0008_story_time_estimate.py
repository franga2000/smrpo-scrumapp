# Generated by Django 5.0.2 on 2024-03-18 19:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrumapp', '0007_alter_project_name_alter_story_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='story',
            name='time_estimate',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
    ]
