# Generated by Django 5.0.3 on 2024-04-23 23:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrumapp', '0020_remove_task_started_trackedtime_started_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='soft_deleted',
            field=models.BooleanField(default=False),
        ),
    ]
