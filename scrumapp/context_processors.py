from django.http import HttpRequest

from scrumapp.models import Project, TrackedTime


# Add the (visible) project list to all pages
# TODO: cache this
def project_list_context(request: HttpRequest):
    ctx = {}
    if request.user.is_authenticated:
        ctx['projects'] = Project.objects.filter(projectmember__user=request.user).distinct()
        ctx['active_trackedtime'] = TrackedTime.objects.filter(
            user=request.user,
            started__isnull=False,
        ).first()
    return ctx
