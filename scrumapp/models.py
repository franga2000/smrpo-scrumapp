from functools import cached_property

from django.contrib.auth.models import User
from django.db import models
from django.forms import ValidationError
from django.urls import reverse_lazy
from django.utils import timezone

from scrumapp.validators import date_not_in_past


class RoleEnum(models.TextChoices):
    DEVELOPER = "DEV", "Developer"
    SCRUM_MASTER = "SM", "Scrum Master"
    PRODUCT_OWNER = "PO", "Product Owner"

class ProjectMember(models.Model):
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    project = models.ForeignKey("Project", on_delete=models.CASCADE)
    role = models.CharField(max_length=20, choices=RoleEnum.choices)

class Project(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)

    sprints: 'SprintQuerySet'
    stories: 'StoryQuerySet'

    def __str__(self):
        return self.name
    
    # These use .all() to take advantage of query caching, but we still need to confirm queries are actually cached
    
    @cached_property
    def product_owner(self) -> User:
        for pm in self.projectmember_set.all():
            if pm.role == RoleEnum.PRODUCT_OWNER:
                return pm.user
            
    @cached_property
    def scrum_master(self) -> User:
        for pm in self.projectmember_set.all():
            if pm.role == RoleEnum.SCRUM_MASTER:
                return pm.user
    
    @cached_property
    def developers(self) -> list[User]:
        return [pm.user for pm in self.projectmember_set.all() if pm.role == RoleEnum.DEVELOPER]

    def get_absolute_url(self):
        return reverse_lazy("project_detail", kwargs={"project_id": self.id})
    

# A custom manager for Sprint so we can DRY our queries
class SprintQuerySet(models.QuerySet):
    def active(self):
        now = timezone.now()
        return self.filter(start_date__lte=now, end_date__gte=now)

    def future(self):
        return self.filter(start_date__gt=timezone.now())
    
    def past(self):
        return self.filter(end_date__lt=timezone.now())


class Sprint(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="sprints")
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    velocity = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name="Velocity (pts)")

    objects = SprintQuerySet.as_manager()

    def __str__(self):
        return self.name

    def clean(self):
        if self.start_date > self.end_date:
            raise ValidationError({
                "start_date": "Start date cannot be after end date"
            })
        # Only run this validation on new objects (when creating)
        if self.id is None:
            if self.start_date < timezone.now().date():
                raise ValidationError({
                    "start_date": "Start date cannot be in the past"
                })

        # TODO: turn this into a field validation
        if self.velocity == 0:
            raise ValidationError({
                "velocity": "Velocity cannot be 0"
            })

    def get_absolute_url(self):
        return reverse_lazy("sprint_detail", kwargs={"project_id": self.project.id, "pk": self.id})

    def is_future(self):
        return self.start_date > timezone.now().date()

    def is_active(self):
        return self.start_date <= timezone.now().date() and \
            self.end_date >= timezone.now().date()


class StoryQuerySet(models.QuerySet):
    def product_backlog(self):
        return (self.filter(sprint=None)
            .exclude(status=Story.Status.ACCEPTED) |
            self.filter(status=Story.Status.REJECTED)) \
            .exclude(priority=Story.Priority.WONT_HAVE)


class Story(models.Model):
    class Priority(models.IntegerChoices):
        MUST_HAVE = 1, 'Must have'
        SHOULD_HAVE = 2, 'Should have'
        COULD_HAVE = 3, 'Could have'
        WONT_HAVE = 4, "Won't have at this time"

    class Status(models.IntegerChoices):
        UNKNOWN = 0, 'Unknown'
        ACCEPTED = 1, 'Accepted'
        REJECTED = 2, 'Rejected'

    objects = StoryQuerySet.as_manager()

    sprint = models.ForeignKey(Sprint, null=True, blank=True, on_delete=models.SET_NULL)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="stories")
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    acceptance_tests = models.TextField(blank=True)
    priority = models.IntegerField(choices=Priority)
    status = models.IntegerField(choices=Status, default=Status.UNKNOWN)
    business_value = models.FloatField(verbose_name="Business value")
    time_estimate = models.FloatField(verbose_name="Time estimate (pts)", null=True, blank=True)
    finished = models.BooleanField(default=False)
    soft_deleted = models.BooleanField(default=False)

    tasks: 'models.QuerySet[Task]'
    notes: 'models.QuerySet[StoryNotes]'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.id})

    def is_accepted(self):
        return self.status == Story.Status.ACCEPTED

    def is_recjected(self):
        return self.status == Story.Status.REJECTED

    def clean(self):
        if self.business_value is not None:
            if self.business_value <= 0 or self.business_value > 10:
                raise ValidationError({
                    "business_value": "Business value should be between 1 and 10"
                })

    @cached_property
    def is_finished(self):
        finished_tasks_count = self.tasks.filter(status=Task.Status.FINISHED).count()
        return finished_tasks_count == self.tasks.count() and finished_tasks_count > 0
    
    class Meta:
        verbose_name_plural = "stories"

class StoryNotes(models.Model):
    story = models.ForeignKey(Story, on_delete=models.CASCADE, related_name="notes")
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    content = models.TextField()
    time_posted = models.DateTimeField(auto_now_add=True)

class Task(models.Model):
    class Status(models.IntegerChoices):
        UNCLAIMED = 0, 'Unclaimed'
        CLAIMED = 1, 'Claimed'
        ACTIVE = 2, 'Active'
        FINISHED = 3, 'Finished'

    story = models.ForeignKey(Story, on_delete=models.CASCADE, related_name="tasks")
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    status = models.IntegerField(choices=Status, default=Status.UNCLAIMED)
    assigned_to = models.ForeignKey("auth.User", on_delete=models.SET_NULL, null=True, blank=True)
    hours = models.IntegerField()
    date = models.DateField(default=timezone.now)
    soft_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class TrackedTime(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="work_logs")
    date = models.DateField(default=timezone.now)
    # Time when the task was started (if not running, this is None)
    started = models.DateTimeField(null=True, blank=True)
    hours = models.FloatField(null=True, blank=True)
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)

    class Meta:
        unique_together = ('task', 'date', 'user')


class Post(models.Model):
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    time_posted = models.DateTimeField(auto_now_add=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy("post_list", kwargs={"project_id": self.project.id})
    

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    content = models.TextField()
    time_posted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Comment by {self.user.username} on {self.post.title}"
