import time

from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from django.contrib.auth.models import User
from selenium.webdriver.support.ui import Select
from scrumapp import models

from selenium.webdriver.common.action_chains import ActionChains

adminUn="testAdmin"
adminPass="adminTestTest"

un = "TestUser"
passw = "testtesttest"

driver = webdriver.Firefox()
driver.implicitly_wait(5)



class AFinishingTasks(LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url + '/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer", "dev@test.com", passw)
		po = User.objects.get(username="ProductOwner").id
		sm = User.objects.get(username="ScrumMaster").id
		dev = User.objects.get(username="Developer").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid = models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId= models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id
		#Accepted story
		models.Story.objects.create(title="AcceptedStory", sprint_id=sprintId, project_id=projectid, priority=1, status=1, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyAcceptedId = models.Story.objects.get(title="AcceptedStory").id
		#Rejected story
		models.Story.objects.create(title="RejectedStory", project_id=projectid, priority=1, status=2, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyRejectedId = models.Story.objects.get(title="RejectedStory").id

		#Create task
		models.Task.objects.create(title="TestTask",hours=2,assigned_to_id=dev,story_id=storyId,status=1)
		taskId=models.Task.objects.get(title="TestTask").id
		#Create task accepted story
		models.Task.objects.create(title="TestTaskAccepted",hours=2,assigned_to_id=dev,story_id=storyAcceptedId,status=3)
		taskAcceptedId=models.Task.objects.get(title="TestTaskAccepted").id
		#Create task rejected story
		models.Task.objects.create(title="TestTaskRejected",hours=2,assigned_to_id=dev,story_id=storyRejectedId,status=1)
		taskRejectedId=models.Task.objects.get(title="TestTaskRejected").id

		#Login
		username = driver.find_element(By.ID, "id_username")
		password = driver.find_element(By.ID, 'id_password')
		submit = driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer")
		password.send_keys(passw)
		submit.click()
	def testA1FinishingTask(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()
		#Navigate to In sprint
		inSprint=driver.find_element(By.ID,"tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR,"#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID,"nav-claimed-tab")
		claimed.click()

		#Finished button
		finButton=driver.find_element(By.CSS_SELECTOR, "#nav-claimed > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > form:nth-child(3) > button:nth-child(2)")
		finButton.click()

		#Navigate to Finished
		finished=driver.find_element(By.ID,"nav-finished-tab")
		finished.click()

		assert "TestTask" in driver.page_source

	def testA2AcceptedStory(self):
		#Open project
		openProject = driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Accepted
		accepted=driver.find_element(By.ID,"tabs2-3-nav")
		accepted.click()

		#Open story
		openStory = driver.find_element(By.CSS_SELECTOR,"#tabs2-3 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Finished
		finished = driver.find_element(By.ID, "nav-finished-tab")
		finished.click()

		assert "Finish" not in driver.find_element(By.CSS_SELECTOR,"#nav-finished > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)").text

	def testA3RejectedStory(self):
		#Open project
		openProject = driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Open
		open=driver.find_element(By.ID,"tabs2-1-nav")
		open.click()

		#Open story
		openStory = driver.find_element(By.CSS_SELECTOR,"div.row:nth-child(2) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed = driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		assert "Finish" not in driver.find_element(By.CSS_SELECTOR,"#nav-claimed > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)").text

class BAcceptingStories(LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url + '/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer", "dev@test.com", passw)
		po = User.objects.get(username="ProductOwner").id
		sm = User.objects.get(username="ScrumMaster").id
		dev = User.objects.get(username="Developer").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid = models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId = models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId = models.Story.objects.get(title="TestStory").id
		#Unfinished story
		models.Story.objects.create(title="UnfinishedStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyUnfinishedId = models.Story.objects.get(title="UnfinishedStory").id
		#Accepted story
		models.Story.objects.create(title="AcceptedStory", sprint_id=sprintId, project_id=projectid, priority=1, status=1, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyAcceptedId=models.Story.objects.get(title="AcceptedStory").id
		#Rejected story
		models.Story.objects.create(title="RejectedStory", project_id=projectid, priority=1, status=2, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyRejectedId=models.Story.objects.get(title="RejectedStory").id

		#Create task
		models.Task.objects.create(title="TestTask", hours=2, assigned_to_id=dev, story_id=storyUnfinishedId, status=3)
		taskId = models.Task.objects.get(title="TestTask").id
		#Create task unfinished
		models.Task.objects.create(title="TestTaskUnfinished", hours=2, assigned_to_id=dev, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskUnfinished").id

		#Login
		username = driver.find_element(By.ID, "id_username")
		password = driver.find_element(By.ID, 'id_password')
		submit = driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("ProductOwner")
		password.send_keys(passw)
		submit.click()

	def testB1AcceptStory(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Active sprint
		activeSprint=driver.find_element(By.CSS_SELECTOR, "#nav-active > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		activeSprint.click()

		#Accept/Reject
		accButton=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(2)")
		accButton.click()

		#Status
		status=Select(driver.find_element(By.ID,"id_status"))
		status.select_by_visible_text("Accepted")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(3)")
		save.click()

		#Navigate to Accepted
		accepted=driver.find_element(By.ID,"tabs2-3-nav")
		accepted.click()

		assert "TestStory" in driver.page_source

	def testB2AlreadyAcceptedStory(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Active sprint
		activeSprint=driver.find_element(By.CSS_SELECTOR, "#nav-active > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		activeSprint.click()

		assert "Accept/Reject" not in driver.find_element(By.CSS_SELECTOR,"div.col-sm-5:nth-child(1) > div:nth-child(1) > a:nth-child(1) > div:nth-child(1)").text

	def testB3OpenStory(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Open
		open=driver.find_element(By.ID, "tabs2-1-nav")
		open.click()

		#Story
		story=driver.find_element(By.CSS_SELECTOR,"div.row:nth-child(2) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		story.click()

		assert "Accept/Reject" not in driver.page_source

	def testB4RejectedStory(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Open
		open=driver.find_element(By.ID, "tabs2-1-nav")
		open.click()

		assert "Accept/Reject" not in driver.page_source
	def testB5UnfinishedStory(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Active sprint
		activeSprint=driver.find_element(By.CSS_SELECTOR, "#nav-active > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		activeSprint.click()

		assert "Accept/Reject" not in driver.find_element(By.CSS_SELECTOR,"div.col-sm-5:nth-child(2) > div:nth-child(1)").text

class CRejectingStories (LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer", "dev@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev=User.objects.get(username="Developer").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id
		#Unfinished story
		models.Story.objects.create(title="UnfinishedStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyUnfinishedId=models.Story.objects.get(title="UnfinishedStory").id
		#Accepted story
		models.Story.objects.create(title="AcceptedStory", sprint_id=sprintId, project_id=projectid, priority=1, status=1, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyAcceptedId=models.Story.objects.get(title="AcceptedStory").id
		#Rejected story
		models.Story.objects.create(title="RejectedStory", sprint_id=sprintId, project_id=projectid, priority=1, status=2, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyRejectedId=models.Story.objects.get(title="RejectedStory").id

		#Create task
		models.Task.objects.create(title="TestTask", hours=2, assigned_to_id=dev, story_id=storyUnfinishedId, status=3)
		taskId=models.Task.objects.get(title="TestTask").id
		#Create task unfinished
		models.Task.objects.create(title="TestTaskUnfinished", hours=2, assigned_to_id=dev, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskUnfinished").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("ProductOwner")
		password.send_keys(passw)
		submit.click()
	def testC1RejectStory(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Active sprint
		activeSprint=driver.find_element(By.CSS_SELECTOR, "#nav-active > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		activeSprint.click()

		#Accept/Reject
		accButton=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(2)")
		accButton.click()

		#Status
		status=Select(driver.find_element(By.ID, "id_status"))
		status.select_by_visible_text("Rejected")

		#Save
		save=driver.find_element(By.CSS_SELECTOR, "button.btn:nth-child(3)")
		save.click()

		#Navigate to Open
		open=driver.find_element(By.ID, "tabs2-1-nav")
		open.click()

		assert "TestStory" in driver.page_source

	def testC2AlreadyRejected(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Active sprint
		activeSprint=driver.find_element(By.CSS_SELECTOR, "#nav-active > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		activeSprint.click()

		assert "Accept/Reject" not in driver.find_element(By.CSS_SELECTOR,"div.card-body:nth-child(2)").text

	def testC3OpenStoryReject(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Open
		open=driver.find_element(By.ID, "tabs2-1-nav")
		open.click()

		#Story
		story=driver.find_element(By.CSS_SELECTOR,"div.row:nth-child(2) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		story.click()

		assert "Accept/Reject" not in driver.page_source

	def testC4AcceptedStoryReject(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Accepted
		accepted=driver.find_element(By.ID, "tabs2-3-nav")
		accepted.click()
		#Story
		story=driver.find_element(By.CSS_SELECTOR,"#tabs2-3 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		story.click()

		assert "Accept/Reject" not in driver.page_source

class DTaskList (LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		User.objects.create_user("Developer2", "dev2@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id
		dev2=User.objects.get(username="Developer2").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="DEV", user_id=dev2, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id


		#Create task - unassigned
		models.Task.objects.create(title="TestTaskUnassigned1", hours=2, story_id=storyId, status=0)
		taskId=models.Task.objects.get(title="TestTaskUnassigned1").id
		#Create task - unassigned
		models.Task.objects.create(title="TestTaskUnassigned2", hours=2,  story_id=storyId, status=0)
		taskId=models.Task.objects.get(title="TestTaskUnassigned2").id
		#Create task - assigned to dev1
		models.Task.objects.create(title="TestTaskDev1-1", hours=2, assigned_to_id=dev1, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskDev1-1").id
		#Create task - assugned to dev1
		models.Task.objects.create(title="TestTaskDev1-2", hours=2, assigned_to_id=dev1, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskDev1-2").id
		#Create task - assugned to dev2
		models.Task.objects.create(title="TestTaskDev2-1", hours=2, assigned_to_id=dev2, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskDev2-1").id
		#Create task - assugned to dev2
		models.Task.objects.create(title="TestTaskDev2-2", hours=2, assigned_to_id=dev2, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskDev2-2").id
		#Create task - finished
		models.Task.objects.create(title="TestTaskFinished1", hours=2, assigned_to_id=dev1, story_id=storyId, status=3)
		taskId=models.Task.objects.get(title="TestTaskFinished1").id
		#Create task - finished
		models.Task.objects.create(title="TestTaskFinished2", hours=2, assigned_to_id=dev2, story_id=storyId, status=3)
		taskId=models.Task.objects.get(title="TestTaskFinished2").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer1")
		password.send_keys(passw)
		submit.click()

	def testD1Unclaimed(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Unclaimed
		unclaimed=driver.find_element(By.ID,"nav-unclaimed-tab")
		unclaimed.click()

		assert "TestTaskUnassigned1" in driver.page_source and "TestTaskUnassigned2" in driver.page_source

	def testD2AssignedToDev1(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID,"nav-claimed-tab")
		claimed.click()

		assert "TestTaskDev1-1" in driver.page_source and "TestTaskDev1-2" in driver.page_source

	def testD3AssignedToDev2(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		assert "TestTaskDev2-1" in driver.page_source and "TestTaskDev2-2" in driver.page_source

	def testD4Finished(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Unclaimed
		finished=driver.find_element(By.ID,"nav-finished-tab")
		finished.click()

		assert "TestTaskFinished1" in driver.page_source and "TestTaskFinished2" in driver.page_source

	def testD5InProgress(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID,"nav-claimed-tab")
		claimed.click()

		#Start tracking
		track=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(2)")
		track.click()

		#Navigate to In progress
		inProgress=driver.find_element(By.ID,"nav-active-tab")
		inProgress.click()

		assert "TestTaskDev1-1" in driver.page_source

class ESprintMaintainence(LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		User.objects.create_user("Developer2", "dev2@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		models.Sprint.objects.create(name="TestSprintDuplicate", start_date="2024-07-30", end_date="2024-08-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprintDuplicate").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("ScrumMaster")
		password.send_keys(passw)
		submit.click()

	def testE1EndBeforeStart(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Future sprints
		futureSprints=driver.find_element(By.ID, "nav-future-tab")
		futureSprints.click()

		#Select sprint
		sprint=driver.find_element(By.CSS_SELECTOR,"#nav-future > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		sprint.click()

		#Edit
		edit=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(5)")
		edit.click()

		#Change end date
		endDate=driver.find_element(By.ID,"id_end_date")
		ActionChains(driver).move_to_element(endDate).click().send_keys('05102024').perform()

		velocity=driver.find_element(By.ID,"id_velocity")
		velocity.clear()
		velocity.send_keys("7")

		#Save
		save=driver.find_element(By.CSS_SELECTOR, "button.btn:nth-child(7)")
		save.click()

		assert "Start date cannot be after end date" in driver.page_source

	def testE2Overlap(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Future sprints
		futureSprints=driver.find_element(By.ID, "nav-future-tab")
		futureSprints.click()

		#Select sprint
		sprint=driver.find_element(By.CSS_SELECTOR,"#nav-future > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		sprint.click()

		#Edit
		edit=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(5)")
		edit.click()

		#Change end date
		endDate=driver.find_element(By.ID,"id_end_date")
		ActionChains(driver).move_to_element(endDate).click().send_keys('08102024').perform()

		velocity=driver.find_element(By.ID,"id_velocity")
		velocity.clear()
		velocity.send_keys("7")

		#Save
		save=driver.find_element(By.CSS_SELECTOR, "button.btn:nth-child(7)")
		save.click()
		assert "Sprint overlaps with" in driver.page_source

	def testE3StartInPast(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Future sprints
		futureSprints=driver.find_element(By.ID, "nav-future-tab")
		futureSprints.click()

		#Select sprint
		sprint=driver.find_element(By.CSS_SELECTOR,"#nav-future > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		sprint.click()

		#Edit
		edit=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(5)")
		edit.click()

		#Change end date
		endDate=driver.find_element(By.ID,"id_end_date")
		ActionChains(driver).move_to_element(endDate).click().send_keys('03102024').perform()

		velocity=driver.find_element(By.ID,"id_velocity")
		velocity.clear()
		velocity.send_keys("7")

		#Save
		save=driver.find_element(By.CSS_SELECTOR, "button.btn:nth-child(7)")
		save.click()

		assert "Start date cannot be after end date" in driver.page_source


	def testE4ChangeVelocity(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Future sprints
		futureSprints=driver.find_element(By.ID, "nav-future-tab")
		futureSprints.click()

		#Select sprint
		sprint=driver.find_element(By.CSS_SELECTOR,"#nav-future > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		sprint.click()

		#Edit
		edit=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(5)")
		edit.click()

		#Change Velocity
		velocity=driver.find_element(By.ID,"id_velocity")
		velocity.clear()
		velocity.send_keys("42")

		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(7)")
		save.click()
		assert "42" in driver.page_source

class FRejectingTasks(LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		User.objects.create_user("Developer2", "dev2@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id
		dev2=User.objects.get(username="Developer2").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="DEV", user_id=dev2, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id

		#Create task - unassigned
		models.Task.objects.create(title="TestTaskUnassigned", hours=2, story_id=storyId, status=0)
		taskId=models.Task.objects.get(title="TestTaskUnassigned").id
		#Create task - assigned to dev1
		models.Task.objects.create(title="TestTaskDev1", hours=2, assigned_to_id=dev1, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskDev1").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer1")
		password.send_keys(passw)
		submit.click()

	def testF1UnclaimClaimed(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		#Unclaim
		unclaim=driver.find_element(By.CSS_SELECTOR,"#nav-claimed > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > form:nth-child(2) > button:nth-child(2)")
		unclaim.click()

		#Navigate to Unclaimed
		unclaimed=driver.find_element(By.ID, "nav-unclaimed-tab")
		unclaimed.click()

		assert "TestTaskDev1" in driver.page_source

	def testF2RejectAssigned(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Unclaimed
		unclaimed=driver.find_element(By.ID, "nav-unclaimed-tab")
		unclaimed.click()

		#Edit task
		task=driver.find_element(By.CSS_SELECTOR,"#nav-unclaimed > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > a:nth-child(2)")
		task.click()

		#Change assigned
		assignedTo=Select(driver.find_element(By.ID, "id_assigned_to"))
		assignedTo.select_by_visible_text("---------")

		hours=driver.find_element(By.ID,"id_hours")
		hours.clear()
		hours.send_keys("5")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()

		#Navigate to Unclaimed
		unclaimed=driver.find_element(By.ID, "nav-unclaimed-tab")
		unclaimed.click()

		assert "TestTaskDev1" in driver.page_source

	def testF3Dev2Claim(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		#Unclaim
		unclaim=driver.find_element(By.CSS_SELECTOR, "#nav-claimed > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > form:nth-child(2) > button:nth-child(2)")
		unclaim.click()

		#Logout
		sidebar=driver.find_element(By.CSS_SELECTOR,"a.d-flex")
		sidebar.click()
		logout=driver.find_element(By.CSS_SELECTOR,"button.dropdown-item")
		logout.click()
		driver.get(self.live_server_url+'/')

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer2")
		password.send_keys(passw)
		submit.click()

		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Unclaimed
		unclaimed=driver.find_element(By.ID, "nav-unclaimed-tab")
		unclaimed.click()

		#Claim
		claimBtn=driver.find_element(By.CSS_SELECTOR,"div.card:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > form:nth-child(1) > button:nth-child(2)")
		claimBtn.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		assert "TestTaskDev1" in driver.page_source

class GEditingWall (LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		User.objects.create_user("Developer2", "dev2@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create post
		models.Post.objects.create(title="TestPost",description="Random description", project_id=projectid,user_id=dev1)
		postId=models.Post.objects.get(title="TestPost").id

		#Create comment
		models.Comment.objects.create(content="TestComment",post_id=postId, user_id=dev1)

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("ScrumMaster")
		password.send_keys(passw)
		submit.click()
	def testG1DeleteComment(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Open wall
		wall=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(4)")
		wall.click()

		#Delete button
		delete=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(2)")
		delete.click()
		#Confirm
		confirm=driver.find_element(By.CSS_SELECTOR,".container-fluid > form:nth-child(1) > input:nth-child(3)")
		confirm.click()

		assert "TestComment" not in driver.page_source

	def testG2DeletePost(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Open wall
		wall=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(4)")
		wall.click()

		#Delete button
		delete=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(6)")
		delete.click()

		#Confirm
		confirm=driver.find_element(By.CSS_SELECTOR,".container-fluid > form:nth-child(1) > input:nth-child(3)")
		confirm.click()

		assert "TestPost" not in driver.page_source

class HManagingStories (LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		User.objects.create_user("Developer2", "dev2@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create story in sprint
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id
		#Create story unassigned to sprint
		models.Story.objects.create(title="TestStoryUnassigned1", project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyIdUnass1=models.Story.objects.get(title="TestStoryUnassigned1").id
		#Create story unassigned to sprint
		models.Story.objects.create(title="TestStoryUnassigned2", project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyIdUnass2=models.Story.objects.get(title="TestStoryUnassigned2").id
		#Create story accepted
		models.Story.objects.create(title="TestStoryAccepted", project_id=projectid, priority=1, status=1, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyIdAcc=models.Story.objects.get(title="TestStoryAccepted").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("ScrumMaster")
		password.send_keys(passw)
		submit.click()

	def testH1Unassigned(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR,"div.row:nth-child(2) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Edit
		editBtn=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(10)")
		editBtn.click()

		#Title
		title=driver.find_element(By.ID,"id_title")
		title.send_keys("ChangedTitle")

		#ScrollHack
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		time.sleep(2)

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(8)")
		save.click()

		assert "ChangedTitle" in driver.page_source

	def testH2InSprint(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Edit
		editBtn=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(10)")
		editBtn.click()

		#Title
		title=driver.find_element(By.ID, "id_title")
		title.send_keys("ChangedTitle")

		#ScrollHack
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		time.sleep(2)

		#Save
		save=driver.find_element(By.CSS_SELECTOR, "button.btn:nth-child(8)")
		save.click()

		assert "Cannot change story in the active sprint" in driver.page_source
	def testH3Accepted(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to Accepted
		accepted=driver.find_element(By.ID, "tabs2-3-nav")
		accepted.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-3 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Edit
		editBtn=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(10)")
		editBtn.click()

		#Title
		title=driver.find_element(By.ID, "id_title")
		title.send_keys("ChangedTitle")

		#ScrollHack
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		time.sleep(2)

		#Save
		save=driver.find_element(By.CSS_SELECTOR, "button.btn:nth-child(8)")
		save.click()

		assert "500 Server Error" in driver.page_source

	def testH4Duplicate(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR,"div.row:nth-child(2) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Edit
		editBtn=driver.find_element(By.CSS_SELECTOR,"a.btn:nth-child(10)")
		editBtn.click()

		#Title
		title=driver.find_element(By.ID,"id_title")
		title.clear()
		title.send_keys("TestStoryUnassigned2")

		#ScrollHack
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		time.sleep(2)

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(8)")
		save.click()

		assert "Story with this title already exists in the project" in driver.page_source

class IManagingTasks (LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		User.objects.create_user("Developer2", "dev2@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id
		dev2=User.objects.get(username="Developer2").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="DEV", user_id=dev2, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id

		#Create task - unassigned
		models.Task.objects.create(title="TestTaskUnassigned", hours=2, story_id=storyId, status=0)
		taskId=models.Task.objects.get(title="TestTaskUnassigned").id
		#Create task - assigned to dev1
		models.Task.objects.create(title="TestTaskDev1", hours=2, assigned_to_id=dev1, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskDev1").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer1")
		password.send_keys(passw)
		submit.click()

	def testI1EditTask(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Unclaimed
		unclaimed=driver.find_element(By.ID, "nav-unclaimed-tab")
		unclaimed.click()

		#Edit
		editBtn=driver.find_element(By.CSS_SELECTOR,"a.btn-warning:nth-child(2)")
		editBtn.click()

		#Title
		title=driver.find_element(By.ID,"id_title")
		title.send_keys("NewTitle")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()

		#Navigate to Unclaimed
		unclaimed=driver.find_element(By.ID, "nav-unclaimed-tab")
		unclaimed.click()

		assert "NewTitle" in driver.page_source

	def testI2DeleteClaimed(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		assert "Delete" not in driver.find_element(By.CSS_SELECTOR,"#nav-unclaimed > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)").text

class JUserData (LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_user("Developer1", "dev@test.com", passw)
		User.objects.create_user("Developer2", "dev2@test.com", passw)
		dev1=User.objects.get(username="Developer1").id
		dev2=User.objects.get(username="Developer2").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer1")
		password.send_keys(passw)
		submit.click()

	def testJ1ChangeUsername(self):
		#Dropdown
		dropdown=driver.find_element(By.CSS_SELECTOR,"a.d-flex")
		dropdown.click()

		#Profile
		profile=driver.find_element(By.CSS_SELECTOR,"a.dropdown-item")
		profile.click()

		#Username
		un=driver.find_element(By.ID,"id_username")
		un.send_keys("NewUsername")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()

		assert "Your profile is updated successfully" in driver.page_source

	def testJ2DuplicateUsername(self):
		#Dropdown
		dropdown=driver.find_element(By.CSS_SELECTOR,"a.d-flex")
		dropdown.click()

		#Profile
		profile=driver.find_element(By.CSS_SELECTOR,"a.dropdown-item")
		profile.click()

		#Username
		un=driver.find_element(By.ID,"id_username")
		un.clear()
		un.send_keys("Developer2")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()

		assert "Uporabnik s tem uporabniškim imenom že obstaja." in driver.page_source

	def testJ3ChangePassword(self):
		#Dropdown
		dropdown=driver.find_element(By.CSS_SELECTOR,"a.d-flex")
		dropdown.click()

		#Profile
		profile=driver.find_element(By.CSS_SELECTOR,"a.dropdown-item")
		profile.click()

		#ChangePass
		changePass=driver.find_element(By.CSS_SELECTOR,"a.btn-secondary")
		changePass.click()

		#OldPass
		oldPass=driver.find_element(By.ID,"id_old_password")
		oldPass.send_keys(passw)

		#NewPass
		newPass=driver.find_element(By.ID,"id_new_password1")
		newPass.send_keys("NewPassword123")

		#ConfirmPass
		confPass=driver.find_element(By.ID,"id_new_password2")
		confPass.send_keys("NewPassword123")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(8)")
		save.click()

		assert "Successfully Changed Your Password" in driver.page_source

	def testJ4ChangeData(self):
		#Dropdown
		dropdown=driver.find_element(By.CSS_SELECTOR, "a.d-flex")
		dropdown.click()

		#Profile
		profile=driver.find_element(By.CSS_SELECTOR, "a.dropdown-item")
		profile.click()

		#Email
		email=driver.find_element(By.ID, "id_email")
		email.clear()
		email.send_keys("new@email.dev")

		#FirstName
		fname=driver.find_element(By.ID, "id_first_name")
		fname.send_keys("Test")

		#LastName
		lname=driver.find_element(By.ID, "id_last_name")
		lname.send_keys("Developer")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()

		assert "Your profile is updated successfully" in driver.page_source

class KStoryNotes(LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id

		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer1")
		password.send_keys(passw)
		submit.click()

	def testK1AddNote(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Add note
		note=driver.find_element(By.CSS_SELECTOR,".form-control")
		note.send_keys("New note")
		note.submit()
		time.sleep(2)

		assert "New note" in driver.page_source

#class LDocumentation(LiveServerTestCase):
#	def setUp(self):
#		#Start selenium browser
#		driver.get(self.live_server_url+'/')
#
#		#Create users
#		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
#		User.objects.create_user("ProductOwner", "po@test.com", passw)
#		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
#		User.objects.create_user("Developer1", "dev@test.com", passw)
#		po=User.objects.get(username="ProductOwner").id
#		sm=User.objects.get(username="ScrumMaster").id
#		dev1=User.objects.get(username="Developer1").id
#
#		#Create project
#		models.Project.objects.create(name="TestProject")
#		projectid=models.Project.objects.get(name="TestProject").id
#		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
#		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
#		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)
#
#		#Login
#		username=driver.find_element(By.ID, "id_username")
#		password=driver.find_element(By.ID, 'id_password')
#		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
#		username.send_keys("Developer1")
#		password.send_keys(passw)
#		submit.click()
#
#	def testL1EditDocumentation(self):
#		#Open project
#		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
#		openProject.click()
#
#		#Open docs
#		docs=driver.find_element(By.CSS_SELECTOR, "docs-button")
#		docs.click()
#
#		textField=driver.find_element(By.ID, "id_doc")
#		textField.send_keys("Edit doc")
#		save=driver.find_element(By.CSS_SELECTOR, "save-button")
#		save.click()
#
#		assert "Edit doc" in driver.page_source
#
#	def testL2Export(self):
#		#Open project
#		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
#		openProject.click()
#
#		#Open docs
#		docs=driver.find_element(By.CSS_SELECTOR, "docs-button")
#		docs.click()
#
#		#Export
#		export=driver.find_element(By.CSS_SELECTOR,"export-button")
#		export.click()
#
#		assert "Documentation exported" in driver.page_source
#
#	def testL3Import(self):
#		#Open project
#		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
#		openProject.click()
#
#		#Open docs
#		docs=driver.find_element(By.CSS_SELECTOR, "docs-button")
#		docs.click()
#
#		#Import
#		export=driver.find_element(By.CSS_SELECTOR, "export-button")
#		export.click()
#
#		assert "Documentation imported" in driver.page_source

class MTimeEditing(LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id
		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)

		#Create sprint
		models.Sprint.objects.create(name="TestSprint", start_date="2024-03-30", end_date="2025-04-30", project_id=projectid, velocity="20")
		sprintId=models.Sprint.objects.get(name="TestSprint").id

		#Create story
		models.Story.objects.create(title="TestStory", sprint_id=sprintId, project_id=projectid, priority=1, status=0, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyId=models.Story.objects.get(title="TestStory").id

		#Create story
		models.Story.objects.create(title="TestStoryAccepted", sprint_id=sprintId, project_id=projectid, priority=1, status=1, time_estimate=2, acceptance_tests="Test1", business_value=2)
		storyAccId=models.Story.objects.get(title="TestStory").id

		#Create task - unassigned
		models.Task.objects.create(title="TestTaskUnassigned", hours=2, story_id=storyId, status=0)
		taskId=models.Task.objects.get(title="TestTaskUnassigned").id
		#Create task - assigned to dev1
		models.Task.objects.create(title="TestTaskDev1", hours=2, assigned_to_id=dev1, story_id=storyId, status=1)
		taskId=models.Task.objects.get(title="TestTaskDev1").id
		#Create task - assigned to dev1
		models.Task.objects.create(title="TestTaskDev1-Acc", hours=2, assigned_to_id=dev1, story_id=storyAccId, status=3)
		taskId=models.Task.objects.get(title="TestTaskDev1-Acc").id

		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer1")
		password.send_keys(passw)
		submit.click()

	def testM1EditTime(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		#Edit
		edit=driver.find_element(By.CSS_SELECTOR,"a.me-2:nth-child(4)")
		edit.click()

		#Time
		hours=driver.find_element(By.ID,"id_hours")
		hours.clear()
		hours.send_keys("10")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()

		assert "10 h" in driver.page_source
	def testM2WrongTime(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		#Edit
		edit=driver.find_element(By.CSS_SELECTOR,"a.me-2:nth-child(4)")
		edit.click()

		#Time
		hours=driver.find_element(By.ID,"id_hours")
		hours.clear()
		hours.send_keys("abc")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()
		time.sleep(2)
		assert "Please enter a number." in driver.page_source
	def testM3UnassignedTime(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-2-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-2 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Unclaimed
		claimed=driver.find_element(By.ID, "nav-unclaimed-tab")
		claimed.click()

		#Edit
		edit=driver.find_element(By.CSS_SELECTOR,"a.me-2:nth-child(2)")
		edit.click()

		#Time
		hours=driver.find_element(By.ID,"id_hours")
		hours.clear()
		hours.send_keys("10")

		#Save
		save=driver.find_element(By.CSS_SELECTOR,"button.btn:nth-child(6)")
		save.click()

		assert "10 h" in driver.page_source
	def testM4AcceptedTime(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		#Navigate to In sprint
		inSprint=driver.find_element(By.ID, "tabs2-3-nav")
		inSprint.click()

		#Open story
		openStory=driver.find_element(By.CSS_SELECTOR, "#tabs2-3 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
		openStory.click()

		#Navigate to Claimed
		claimed=driver.find_element(By.ID, "nav-claimed-tab")
		claimed.click()

		assert "TestTaskDev1-Acc" not in driver.page_source

class NBurnDown(LiveServerTestCase):
	def setUp(self):
		#Start selenium browser
		driver.get(self.live_server_url+'/')

		#Create users
		User.objects.create_superuser(adminUn, "test@admin.com", adminPass)
		User.objects.create_user("ProductOwner", "po@test.com", passw)
		User.objects.create_user("ScrumMaster", "sm@test.com", passw)
		User.objects.create_user("Developer1", "dev@test.com", passw)
		po=User.objects.get(username="ProductOwner").id
		sm=User.objects.get(username="ScrumMaster").id
		dev1=User.objects.get(username="Developer1").id
		#Create project
		models.Project.objects.create(name="TestProject")
		projectid=models.Project.objects.get(name="TestProject").id
		models.ProjectMember.objects.create(role="DEV", user_id=dev1, project_id=projectid)
		models.ProjectMember.objects.create(role="SM", user_id=sm, project_id=projectid)
		models.ProjectMember.objects.create(role="PO", user_id=po, project_id=projectid)
		#Login
		username=driver.find_element(By.ID, "id_username")
		password=driver.find_element(By.ID, 'id_password')
		submit=driver.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(4)')
		username.send_keys("Developer1")
		password.send_keys(passw)
		submit.click()
	def testN1Diagram(self):
		#Open project
		openProject=driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(1)")
		openProject.click()

		time.sleep(2)

		assert driver.find_element(By.ID,"burndownChart").is_displayed()