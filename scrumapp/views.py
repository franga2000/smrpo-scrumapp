from datetime import date, time
from typing import Any, List
from decimal import Decimal

from django import forms
from django.contrib import messages
from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin)
from django.contrib.auth.models import User
from django.db.models import Q, F
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.views.generic import FormView
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import (BaseFormView, CreateView, DeleteView,
                                       UpdateView)
from django.views.generic.list import ListView
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.formats import localize_input

from scrumapp.formfields import HTML5DateInput
from scrumapp.models import (Post, Project, ProjectMember, RoleEnum, Sprint,
                             Story, Task, TrackedTime, Comment, StoryNotes)


class ProjectMemberMixin(LoginRequiredMixin, PermissionRequiredMixin):
    allowed_roles = ()

    project = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['project'] = self.project
        return context

    def has_permission(self) -> bool:
        if not self.request.user.is_authenticated:
            return False
        
        # (DON'T PLACE ANYTHING ABOVE THIS!)
        # For order-of-execution reasons, we get the project here and save it to the view instance
        project_id = self.kwargs['project_id']
        if project_id is None:
            raise Exception("project_id not found in the URL - did you forget to include it in the URL pattern?")
        self.project = Project.objects.get(id=project_id)

        # Superuser can do anything
        if self.request.user.is_superuser:
            return True

        # Check that the user has one of the allowed roles
        if self.allowed_roles:
            for pm in self.project.projectmember_set.all():
                if pm.user_id == self.request.user.id and pm.role in self.allowed_roles:
                    return True
            return False

        return True


class ProjectList(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = "projects"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        # We're already getting the project list from the context preprocessor
        return {}


class ProjectDetail(LoginRequiredMixin, DetailView):
    model = Project
    context_object_name = "project"
    pk_url_kwarg = "project_id"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active'] = self.object.sprints.active()
        context['future'] = self.object.sprints.future()
        context['past'] = self.object.sprints.past()

        stories = Story.objects.filter(project=self.object, soft_deleted=False)

        context['stories_future'] = stories.filter(priority=Story.Priority.WONT_HAVE)
        context['stories_accepted'] = stories.filter(status=Story.Status.ACCEPTED)
        context['stories_in_sprint'] = stories.filter(~Q(status=Story.Status.ACCEPTED), sprint__in=context['active'])
        context['stories_has_estimate'] = stories.product_backlog().filter(time_estimate__isnull=False)
        context['stories_needs_estimate'] = stories.product_backlog().filter(time_estimate__isnull=True)

        context['add_to_sprint_form'] = StoryAddToSprintForm(project=self.object)

        context['ideal_burn_down'] = self.ideal_burn_down_line_array()
        context['actual_burn_down'] = self.actual_burn_down_line_array()

        return context
    
    def ideal_burn_down_line_array(self):
        sprint = self.object.sprints.active().first()
        stories = Story.objects.filter(project=self.object, sprint=sprint)
        total_hours = sum([story.time_estimate for story in stories.filter(time_estimate__isnull=False)])
        days = (sprint.end_date - sprint.start_date).days
        return [total_hours - (total_hours / days * i) for i in range(days + 1)]
    
    def actual_burn_down_line_array(self):
        sprint: Sprint = self.object.sprints.active().first()
        stories = Story.objects.filter(project=self.object, sprint=sprint)
        total_hours = sum([story.time_estimate for story in stories.filter(time_estimate__isnull=False)])
        days = (sprint.end_date - sprint.start_date).days

        actual_line = [total_hours for _ in range(days + 1)]
        # Subtract work from ideal line
        for i in range(days + 1):
            date = sprint.start_date + timezone.timedelta(days=i)
            total_hours -= sum([tt.hours for tt in TrackedTime.objects.filter(task__story__in=stories, date=date, hours__isnull = False)])
            actual_line[i] = total_hours
        return actual_line

        

       
class ProjectUpdate(ProjectMemberMixin, UpdateView):
    model = Project
    context_object_name = "project"
    template_name_suffix = "_form"
    pk_url_kwarg = "project_id"
    allowed_roles = (RoleEnum.SCRUM_MASTER)

    def get_form(self):
        form = super().get_form()
        form.fields['developers'].initial = User.objects.filter(
            projectmember__project=self.project,
            projectmember__role=RoleEnum.DEVELOPER)
        form.fields['developers'].queryset = User.objects.exclude(
            projectmember__project=self.project,
            projectmember__role=RoleEnum.SCRUM_MASTER).exclude(
            projectmember__project=self.project,
            projectmember__role=RoleEnum.PRODUCT_OWNER)
        return form

    class form_class(forms.ModelForm):
        developers = forms.ModelMultipleChoiceField(queryset=None, required=True)

        class Meta:
            model = Project
            fields = ('name', 'description', 'developers')

        def save(self, commit: bool = ...) -> Any:
            # TODO: transaction
            project = super().save(commit=False)

            users = set(self.cleaned_data['developers'])

            # Very clean set stuff courtesy of Math Man™
            all = set(User.objects.filter(projectmember__project=project, projectmember__role=RoleEnum.DEVELOPER))
            to_add = users - all
            to_remove = all - users
            
            for user in to_add:
                ProjectMember.objects.create(project=project, user=user, role=RoleEnum.DEVELOPER)
            
            ProjectMember.objects.filter(project=project, user__in=to_remove).delete()
            
            return super().save(commit)


class SprintDetail(ProjectMemberMixin, DetailView):
    model = Sprint
    context_object_name = "sprint"

    def get_object(self):
        return Sprint.objects.prefetch_related('story_set__tasks') \
            .order_by('-story__task__status').get(id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        now = date.today()

        ctx['is_active'] = self.object.start_date <= now \
            and self.object.end_date > now

        return ctx


class SprintFormView(ProjectMemberMixin, FormView):
    model = Sprint
    allowed_roles = (RoleEnum.SCRUM_MASTER)

    def get_form(self, form_class=None) -> forms.BaseForm:
        form = super().get_form(form_class)
        form.project = self.project
        form.object = self.object
        return form


    def form_valid(self, form):
        form.instance.project = self.project
        return super().form_valid(form)

    class form_class(forms.ModelForm):
        project: Project
        
        def clean(self):
            cleaned_data = super().clean()
            start_date = cleaned_data.get("start_date")
            if start_date < date.today():
                raise forms.ValidationError('Start date should not be in the past')
            end_date = cleaned_data.get("end_date")
            if end_date <= start_date:
                raise forms.ValidationError("End date should be after start date.")
            object_id = self.object.id if self.object is not None else None
            overlapping_sprint = Sprint.objects.filter(Q(project=self.project),
                ~Q(id=object_id),
                # Start date is before end date is in middle
                Q(start_date__lte=start_date) & Q(end_date__gte=start_date) |
                # Start date and end date are in middle
                Q(start_date__gte=start_date) & Q(end_date__lte=end_date) |
                # Start date is in middle and end date is after
                Q(start_date__lte=end_date) & Q(end_date__gte=end_date)
                ).first()
            if overlapping_sprint:
                raise forms.ValidationError("Sprint overlaps with %(sprint)s [%(start)s - %(end)s]",
                                            params={'sprint': overlapping_sprint.name,
                                                    'start': localize_input(overlapping_sprint.start_date),
                                                    'end': localize_input(overlapping_sprint.end_date)})
            return cleaned_data

        class Meta:
            model = Sprint
            fields = ('name', 'description', 'start_date', 'end_date', 'velocity')

            widgets = {
                'start_date': HTML5DateInput(),
                'end_date': HTML5DateInput(),
            }


class SprintCreate(SprintFormView, CreateView):
    template_name_suffix = '_form'

    def get_initial(self):
        return {'start_date': timezone.now(), 'end_date': timezone.now() + timezone.timedelta(days=7), 'velocity': 1}


class SprintUpdate(SprintFormView, UpdateView):
    template_name_suffix = '_form'

    def clean(self):
        if self.start_date <= date.today() and self.end_date >= date.today():
            raise forms.ValidationError('You cannot edit the active sprint')


class SprintDelete(ProjectMemberMixin, DeleteView):
    model = Sprint
    allowed_roles = (RoleEnum.SCRUM_MASTER)

    def clean(self):
        if self.start_date <= date.today() and self.end_date >= date.today():
            raise forms.ValidationError('You cannot delete the active sprint')

    def get_success_url(self, **kwargs):
        return reverse_lazy('project_detail', kwargs={"project_id": self.project.id})


class StoryDetail(ProjectMemberMixin, DetailView):
    model = Story
    context_object_name = "story"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        story: Story = ctx['object']
        ctx['is_product_owner'] = self.project.product_owner == self.request.user

        tasks = story.tasks.filter(soft_deleted=False)

        ctx['tasks_assigned'] = tasks.filter(status=Task.Status.CLAIMED)
        ctx['tasks_active'] = tasks.filter(status=Task.Status.ACTIVE)
        ctx['tasks_unassigned'] = tasks.filter(status=Task.Status.UNCLAIMED)
        ctx['tasks_finished'] = tasks.filter(status=Task.Status.FINISHED)

        ctx['task_total_hours'] = sum(task.hours for task in story.tasks.all())
        ctx['work_total_hours'] = sum(tt.hours or 0 for tt in TrackedTime.objects.filter(task__in=story.tasks.all()))

        return ctx


class StoryAddToSprintForm(forms.Form):
    stories = forms.ModelMultipleChoiceField(queryset=Story.objects.none(), required=True)

    def __init__(self, *args, **kwargs):
        self.project: Project = kwargs.pop('project')
        super().__init__(*args, **kwargs)
        self.fields['stories'].queryset = self.project.stories.product_backlog()


class AddStoriesToSprintView(ProjectMemberMixin, SingleObjectMixin, BaseFormView):
    model = Project
    pk_url_kwarg = "project_id"
    form_class = StoryAddToSprintForm
    allowed_roles = (RoleEnum.SCRUM_MASTER,)

    def get_form_kwargs(self) -> dict[str, Any]:
        return super().get_form_kwargs() | {'project': self.project}

    def get_success_url(self) -> str:
        return reverse_lazy("project_detail", kwargs={"project_id": self.project.id})
    
    def form_valid(self, form):
        sprint = self.project.sprints.active()[0]
        stories: List[Story] = form.cleaned_data['stories']
        for story in stories:
            story.sprint = sprint
            story.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors)
        return redirect(self.get_success_url())


class StoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.project = kwargs.pop('project')
        self.object = kwargs.pop('story')
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()

        same_title = self.project.stories.filter(title__iexact=cleaned_data['title'])
        
        if self.object:
            same_title = same_title.exclude(id=self.object.id)

            if self.object.sprint is not None and self.object.sprint.end_date > date.today():
                self.add_error(None, 'Cannot change story in the active sprint')
            
            if self.object.status == Story.Status.ACCEPTED:
                self.add_error('status', 'Cannot change accepted story')
        
        if same_title.exists():
            self.add_error('title', 'Story with this title already exists in the project')
        
        return cleaned_data

    class Meta:
        model = Story
        fields = ['title', 'description', 'acceptance_tests', 'priority', 'business_value',]
        widgets = {
            'business_value': forms.NumberInput(attrs={'min': 1, 'max': 10}),
        }


class StoryTimeEstimationForm(StoryForm):
    class Meta:
        model = Story
        fields = ['title', 'description', 'acceptance_tests', 'priority', 'business_value', 'time_estimate']


class StoryFormView(ProjectMemberMixin, FormView):
    model = Story
    form_class = StoryForm
    allowed_roles = (RoleEnum.SCRUM_MASTER, RoleEnum.PRODUCT_OWNER)

    def get_form_kwargs(self) -> dict[str, Any]:
        return super().get_form_kwargs() | {'project': self.project, 'story': self.object}

    def form_valid(self, form):
        form.instance.project = self.project
        return super().form_valid(form)


class StoryCreate(StoryFormView, CreateView):
    template_name_suffix = '_form'

    def get_success_url(self) -> str:
        return reverse_lazy("project_detail", kwargs={"project_id": self.project.id})


class StoryUpdate(StoryFormView, UpdateView):
    template_name_suffix = '_form'

    def get_form_class(self, form_class=None):
        if self.project.scrum_master == self.request.user:
            return StoryTimeEstimationForm
        return StoryForm


class StoryDelete(ProjectMemberMixin, DeleteView):
    model = Story
    allowed_roles = (RoleEnum.PRODUCT_OWNER, RoleEnum.SCRUM_MASTER)

    def get_success_url(self, **kwargs):
        return reverse_lazy('project_detail', kwargs={"project_id": self.project.id})

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.object = self.object
        return form

    def form_valid(self, form):
        work_logs_exist = TrackedTime.objects.filter(task__in=self.object.tasks.all()).exists()
        if work_logs_exist:
            self.object.soft_deleted = True
            self.object.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super().form_valid(form)

    class form_class(forms.ModelForm):
        def clean(self):
            cleaned_data = super().clean()
            if self.object.sprint is not None and self.object.sprint.is_active():
                raise forms.ValidationError('Cannot delete story in the active sprint.')

            if self.object.sprint is not None and self.object.status == Story.Status.ACCEPTED:
                raise forms.ValidationError('Cannot delete accepted story.')

            return cleaned_data

        class Meta:
            model = Story
            fields = ()

class StoryStatusForm(forms.ModelForm):
    class Meta:
        model = Story
        fields = ('status',)


class StoryStatusUpdate(ProjectMemberMixin, UpdateView):
    model = Story
    fields = ('status',)
    template_name_suffix = '_form'
    allowed_roles = (RoleEnum.PRODUCT_OWNER,)

    def get_success_url(self) -> str:
        return reverse_lazy("project_detail", kwargs={"project_id": self.project.id})

class PostFormView(ProjectMemberMixin, FormView):
    model = Post
    fields = ('title', 'description')

    def form_valid(self, form):
        form.instance.project = self.project
        form.instance.user = self.request.user
        return super().form_valid(form)


class PostList(ProjectMemberMixin, ListView):
    model = Post
    context_object_name = "posts"

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(project=self.project).order_by('-time_posted')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        try:
            project_member = ProjectMember.objects.get(user=self.request.user, project=self.project)
            user_role = project_member.role
        except ProjectMember.DoesNotExist:
            user_role = None

        ctx['user_role'] = user_role
        for post in ctx['posts']:
            safe_html = escape(post.description)
            safe_html = safe_html.replace("\n", "<br>")
            post.description = mark_safe(safe_html)
        return ctx

class StoryNoteCreate(LoginRequiredMixin, CreateView):
    model = StoryNotes
    fields = ['content']

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.story = Story.objects.get(id=self.kwargs['story_id'])
        return super().form_valid(form)

    def get_success_url(self) -> str:
        return reverse_lazy("story_detail", kwargs={"project_id": self.kwargs['project_id'] , "pk": self.kwargs['story_id']})
            

class PostCreate(PostFormView, CreateView):
    model = Post
    template_name_suffix = '_form'

    def get_success_url(self) -> str:
        return reverse_lazy("post_list", kwargs={"project_id": self.project.id})


class TaskFormView(ProjectMemberMixin, FormView):
    model = Task
    allowed_roles = (RoleEnum.SCRUM_MASTER, RoleEnum.DEVELOPER)
    fields = ('title', 'description', 'assigned_to', 'hours')

    def form_valid(self, form):
        form.instance.project = self.project
        form.instance.story = Story.objects.get(id=self.kwargs['story_id'])
        return super().form_valid(form)

    def get_success_url(self) -> str:
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.kwargs['story_id']})


class TaskCreate(TaskFormView, CreateView):
    template_name_suffix = '_form'


class TaskUpdate(TaskFormView, UpdateView):
    template_name_suffix = '_form'


class TaskDelete(ProjectMemberMixin, DeleteView):
    model = Task
    allowed_roles = (RoleEnum.SCRUM_MASTER, RoleEnum.DEVELOPER)

    def get_success_url(self, **kwargs):
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.object.story.id})

    def form_valid(self, form):
        if self.object.work_logs.count() > 0:
            success_url = self.get_success_url()
            self.object.soft_deleted = True
            self.object.save()
            return HttpResponseRedirect(success_url)
        else:
            return super().form_valid(form)


class TaskClaim(ProjectMemberMixin, UpdateView):
    model = Task
    fields = ()
    allowed_roles = (RoleEnum.DEVELOPER,)

    def get_success_url(self) -> str:
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.object.story.id})

    def form_valid(self, form):
        form.instance.assigned_to = self.request.user
        form.instance.status = Task.Status.CLAIMED
        return super().form_valid(form)


class TaskUnclaim(ProjectMemberMixin, UpdateView):
    model = Task
    fields = ()
    allowed_roles = (RoleEnum.DEVELOPER,)

    def get_success_url(self) -> str:
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.object.story.id})

    def form_valid(self, form):
        form.instance.assigned_to = None
        form.instance.status = Task.Status.UNCLAIMED
        return super().form_valid(form)


class TaskFinish(ProjectMemberMixin, UpdateView):
    model = Task
    fields = ()
    allowed_roles = (RoleEnum.DEVELOPER,)

    def get_success_url(self) -> str:
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.object.story.id})

    def form_valid(self, form):
        form.instance.status = Task.Status.FINISHED
        return super().form_valid(form)


class TaskTimeStart(ProjectMemberMixin, UpdateView):
    model = Task
    allowed_roles = (RoleEnum.DEVELOPER,)

    def get_success_url(self) -> str:
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.object.story.id})

    def form_valid(self, form):
        tt, _ = TrackedTime.objects.get_or_create(user=self.request.user, task=form.instance, date=timezone.now().date())
        tt.started = timezone.now()
        tt.save()
        form.instance.status = Task.Status.ACTIVE
        return super().form_valid(form)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.request = self.request
        return form

    class form_class(forms.ModelForm):
        def clean(self):
            cleaned_data = super().clean()
            already_tracking = TrackedTime.objects \
                .filter(user=self.request.user, started__isnull=False).exists()
            if already_tracking:
                raise forms.ValidationError("Cannot track more than one task at a time")
            return cleaned_data

        class Meta:
            model = Task
            fields = ()


class TaskTimeStop(ProjectMemberMixin, UpdateView):
    model = Task
    fields = ()
    allowed_roles = (RoleEnum.DEVELOPER,)

    def get_success_url(self) -> str:
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.object.story.id})

    def form_valid(self, form):

        tt = TrackedTime.objects.get(
            task=form.instance,
            user=self.request.user,
            started__isnull=False,
        )

        time_difference = timezone.now() - tt.started
        hours_difference = time_difference.total_seconds() / 3600
        hours_difference_decimal = Decimal(str(hours_difference)).quantize(Decimal('0.01'))

        if tt.hours is None:
            tt.hours = hours_difference_decimal
        else:
            tt.hours += hours_difference_decimal
        tt.started = None
        tt.save()

        form.instance.status = Task.Status.CLAIMED
        return super().form_valid(form)

class LoggedTimeList(ProjectMemberMixin, ListView):
    model = TrackedTime
    context_object_name = "list_time"

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.order_by('-date')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        return ctx
    
class CreateCommentView(LoginRequiredMixin, CreateView):
    model = Comment
    fields = ['content']

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.post = Post.objects.get(id=self.kwargs['post_id'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('post_list', kwargs={'project_id': self.kwargs['project_id']})

class CommentListView(ProjectMemberMixin, ListView):
    model = Comment
    context_object_name = 'comments'

    def get_queryset(self):
        return Comment.objects.filter(post_id=self.kwargs['post_id'])
    
class PostDeleteView(ProjectMemberMixin, DeleteView):
    model = Post
    allowed_roles = (RoleEnum.SCRUM_MASTER,)
    
    def get_success_url(self):
        return reverse_lazy('post_list', kwargs={'project_id': self.kwargs['project_id']})

class CommentDeleteView(ProjectMemberMixin, DeleteView):
    model = Comment
    allowed_roles = (RoleEnum.SCRUM_MASTER,)
    
    def get_success_url(self):
        return reverse_lazy('post_list', kwargs={'project_id': self.kwargs['project_id']})


class UpdateUserForm(forms.ModelForm):
    username = forms.CharField(max_length=100, required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(required=True,
                             widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(max_length=100, required=False,
                                 widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=100, required=False,
                                widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']

class ProfileUpdateView(LoginRequiredMixin, FormView):
    template_name = 'scrumapp/update_profile.html'
    form_class = UpdateUserForm
    success_url = reverse_lazy('update_profile')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Your profile is updated successfully')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Please correct the error below.')
        return super().form_invalid(form)

class ChangePasswordView(SuccessMessageMixin, PasswordChangeView):
    template_name = 'scrumapp/change_passowrd.html'
    success_message = "Successfully Changed Your Password"
    success_url = reverse_lazy('update_profile')