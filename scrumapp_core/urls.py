"""
URL configuration for scrumapp_core project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings

from scrumapp.views import AddStoriesToSprintView, PostList, PostCreate, ProjectDetail, ProjectList, ProjectUpdate, SprintCreate, SprintDetail, SprintUpdate, SprintDelete, StoryCreate, StoryDetail, StoryStatusUpdate, StoryUpdate, StoryDelete, TaskClaim, TaskCreate, TaskUpdate, TaskDelete, TaskTimeStop, TaskTimeStart, TaskUnclaim, TaskFinish, LoggedTimeList, CommentListView, CreateCommentView, ProfileUpdateView, ChangePasswordView,PostDeleteView, CommentDeleteView, StoryNoteCreate

urlpatterns = [
    path('projects', ProjectList.as_view(), name='project_list'),
    path('projects/<int:project_id>', ProjectDetail.as_view(), name='project_detail'),
    path('projects/<int:project_id>/edit', ProjectUpdate.as_view(), name='project_edit'),
    path('projects/<int:project_id>/add_stories_to_sprint', AddStoriesToSprintView.as_view(), name='add_stories_to_sprint'),

    path('projects/<int:project_id>/sprints/<int:pk>', SprintDetail.as_view(), name='sprint_detail'),
    path('projects/<int:project_id>/sprints/create', SprintCreate.as_view(), name='sprint_create'),
    path('projects/<int:project_id>/sprints/<int:pk>/edit', SprintUpdate.as_view(), name='sprint_edit'),
    path('projects/<int:project_id>/sprints/<int:pk>/delete', SprintDelete.as_view(), name='sprint_delete'),

    path('projects/<int:project_id>/stories/<int:pk>', StoryDetail.as_view(), name='story_detail'),
    path('projects/<int:project_id>/stories/create', StoryCreate.as_view(), name='story_create'),
    path('projects/<int:project_id>/stories/<int:pk>/edit', StoryUpdate.as_view(), name='story_edit'),
    path('projects/<int:project_id>/stories/<int:pk>/status', StoryStatusUpdate.as_view(), name='story_status'),
    path('projects/<int:project_id>/stories/<int:pk>/delete', StoryDelete.as_view(), name='story_delete'),
    path('projects/<int:project_id>/stories/<int:story_id>/create_note', StoryNoteCreate.as_view(), name='story_note'),
    

    path('projects/<int:project_id>/stories/<int:story_id>/tasks/create', TaskCreate.as_view(), name='task_create'),
    path('projects/<int:project_id>/stories/<int:story_id>/tasks/<int:pk>/edit', TaskUpdate.as_view(), name='task_edit'),
    path('projects/<int:project_id>/stories/<int:story_id>/tasks/<int:pk>/delete', TaskDelete.as_view(), name='task_delete'),
    path('projects/<int:project_id>/stories/<int:story_id>/tasks/<int:pk>/claim', TaskClaim.as_view(), name='task_claim'),

    path('projects/<int:project_id>/stories/<int:story_id>/tasks/<int:pk>/start_tracking', TaskTimeStart.as_view(), name='task_start'),
    path('projects/<int:project_id>/stories/<int:story_id>/tasks/<int:pk>/stop_tracking', TaskTimeStop.as_view(), name='task_stop'),
    path('projects/<int:project_id>/stories/<int:story_id>/tasks/<int:pk>/unclaim', TaskUnclaim.as_view(), name='task_unclaim'),
    path('projects/<int:project_id>/stories/<int:story_id>/tasks/<int:pk>/finish', TaskFinish.as_view(), name='task_finish'),
    path('projects/<int:project_id>/stories/<int:pk>/list_time', LoggedTimeList.as_view(), name='tracked_time'),
    

    path('projects/<int:project_id>/posts/create', PostCreate.as_view(), name='post_create'),
    path('projects/<int:project_id>/posts', PostList.as_view(), name='post_list'),

    path('projects/<int:project_id>/posts/<int:post_id>/comments/', CommentListView.as_view(), name='comment_list'),
    path('projects/<int:project_id>/posts/<int:post_id>/comments/create/', CreateCommentView.as_view(), name='create_comment'),
    path('projects/<int:project_id>/post/<int:pk>/delete/', PostDeleteView.as_view(), name='post_delete'),
    path('projects/<int:project_id>/comment/<int:pk>/delete/', CommentDeleteView.as_view(), name='comment_delete'),

    path('update-profile/', ProfileUpdateView.as_view(), name='update_profile'),
    path('password-change/', ChangePasswordView.as_view(), name='passwordchange'),

    path("accounts/", include("django.contrib.auth.urls")),
    path('admin/', admin.site.urls),
    path('', lambda r: redirect('project_list'))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Duplicate URL names are allowed by Django, but we don't want them in this project, so make sure they don't happen
names = [pattern.name for pattern in urlpatterns if getattr(pattern, "name", None)]
assert len(names) == len(set(names)), f"Duplicate URL names: {names}"
