FROM python:3.11-slim

WORKDIR /app
EXPOSE ${PORT:-80}

ENV ENVIRONMENT=docker-production

# PIP_NO_CACHE_DIR=off mean NO CACHING. This is stupid, but it's the way it is. See https://github.com/pypa/pip/issues/2897#issuecomment-115319916
RUN PIP_NO_CACHE_DIR=off pip install pipenv==2021.5.29 gunicorn

COPY Pipfile Pipfile.lock ./

RUN PIP_NO_CACHE_DIR=off pipenv install --system --deploy

COPY . .

CMD ["./docker-entrypoint.sh"]
